[root@playbooktab]$ rpm -qa | grep ssl
xmlsec1-openssl-1.2.20-7.el7_4.x86_64
python-backports-ssl_match_hostname-3.4.0.2-4.el7.noarch
openssl-libs-1.0.2k-8.el7.x86_64
openssl-1.0.2k-8.el7.x86_64
[root@playbooktab]$ rpm -qa | grep openssh
openssh-server-7.4p1-12.el7_4.x86_64
openssh-7.4p1-12.el7_4.x86_64
openssh-clients-7.4p1-12.el7_4.x86_64
[root@playbooktab]$